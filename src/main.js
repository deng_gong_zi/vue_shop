import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
//导入全局样式表
import './assets/css/global.css'
//导入字体图标
import './assets/fonts/iconfont.css'
import TreeTable from 'vue-table-with-tree-grid'
import axios from 'axios'
//导入NProgress 包对应的js和css
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
//配置请求的根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'

//在挂载原型对象之前添加拦截请求器
axios.interceptors.request.use((config) => {
  //在request 拦截器中 展示进度条NProgress.start()
  NProgress.start()
  // 获取token
  config.headers.Authorization = window.sessionStorage.getItem('token')
  //在最后必须 return  config  固定写法
  return config
})

//在挂载原型对象之前添加响应拦截器
axios.interceptors.response.use((config) => {
  //在response 拦截器中 展示进度条NProgress.done()
  NProgress.done()
  return config
})

//把包 挂载在vue 的原型对象上给Vue函数添加一个原型属性$axios 指向Axios 每一个组件都是vue的实例对象
//这样做的好处是在vue实例或组件中不用再去重复引用Axios 直接用this.$axios就能执行axios 方法了
//挂载到Vue.prototype上，则其他地方只需要：this.挂载的名称   即可使用  $http 是挂载名称 这样就能使用axios了
Vue.prototype.$http = axios
//阻止启动生产消息，常用作指令。
Vue.config.productionTip = false
//注册为全局组件 展开行的表格
Vue.component('tree-table', TreeTable)

//全局过滤器 vue.filter(过滤名 ， 过滤处理函数  形参1 就是要处理的数据)
Vue.filter('dataFormat', function (oroginVal) {
  //转成时间对象
  const dt = new Date(oroginVal)
  //获取年月日时分秒
  /*- 月份从0开始的 要+1  不足2位 要补0  调用字符串的函数  通过 + “ ” 让其对象变成字符串
之后调用padstart  方法 参数1  总长度多少位 参数2  不足的话 那什么来填充 这里用0来填充 省去了三目判断*/
  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, 0)
  const d = (dt.getDate() + '').padStart(2, 0)
  const hh = (dt.getHours() + '').padStart(2, 0)
  const mm = (dt.getMinutes() + '').padStart(2, 0)
  const ss = (dt.getSeconds() + '').padStart(2, 0)

  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})
new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
