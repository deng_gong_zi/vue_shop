//这是项目发布阶段需要用到的babel插件
const prodPlugins = []
if (process.env.NODE_ENV === 'production') {
  prodPlugins.push('transfrom-remove-console ')
}
module.exports = {
  presets: ['@vue/cli-plugin-babel/preset'],
  plugins: [
    [
      'component',
      {
        libraryName: 'element-ui',
        styleLibraryName: 'theme-chalk',
      },
    ],
    //发布产品时候的插件数组  三个点就是展开运算符  就是把数组的每一项展开了 放到另一个数组中
    ...prodPlugins,
  ],
}
